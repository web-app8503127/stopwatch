let element=document.getElementById("container");
let label=document.createElement("label");
label.innerHTML="00 : 00 : 00";
element.appendChild(label);
element.appendChild(document.createElement("br"));

let seconds=0,hh=0,mm=0,ss=0;
let timer;
let lap=document.createElement("button");
lap.setAttribute("id","lap")
lap.innerHTML="Lap";
lap.disabled=true;
element.appendChild(lap);
lap.addEventListener("click",checkState1);

let start=document.createElement("button");
start.setAttribute("id","start");
start.innerHTML="Start";
element.appendChild(start);
start.addEventListener("click",checkState2);

let ele=document.createElement("div");
ele.setAttribute("id","lapping");
element.appendChild(ele);

let r=1;
function startTimer()
{
  lap.disabled=false;
  timer=setInterval(function()
  {  
    seconds++;
    ss=seconds%60;
    mm=parseInt(seconds/60);
    hh=parseInt(seconds/3600);
    if(mm>59)
      mm%=60;
    if(mm<10)
      mm="0"+mm;
    if(hh<10)
      hh="0"+hh; 
    if(ss<10)
      ss="0"+ss;   
      
    label.innerHTML=hh+" : "+mm+" : "+ss;
  
  },100);
}    
function stopTimer(){
  clearInterval(timer);
  lap.innerHTML="Reset";
}
function checkState2(){
  if(start.innerHTML=="Start")
  {
    start.innerHTML="Stop";
    lap.innerHTML="Lap";
    start.style.backgroundColor="#351614"
    start.style.color="#ff5c33";
    start.style.outline="1px solid #ff5c33";
    startTimer();
  }
  else
  {
    start.innerHTML="Start";
    start.style.color="green";
    start.style.backgroundColor= "#4dff4d";
    start.style.outline= "1px solid #4dff4d";
    stopTimer();
  }
}
function checkState1(){
  if(lap.innerHTML=="Reset")
  {
    label.innerHTML="00 : 00 : 00";
    seconds=0;
    lap.innerHTML="Lap";
    lap.disabled=true;
    r=1;  

    while(ele.lastChild)
    {
      ele.removeChild(ele.lastElementChild);
    }
  }
  else
  {
    let div=document.createElement("div");
    div.setAttribute("class","lapses");
    div.appendChild(document.createElement("hr")); 
    let span1=document.createElement("span");
    span1.innerHTML="Lap "+ r;
    
    let span2=document.createElement("span");
    span2.innerHTML=label.innerHTML;
    span2.setAttribute("class","timing");
    
    div.appendChild(span1);
    div.appendChild(span2);
    
    r++;
    ele.appendChild(div);
  }
}